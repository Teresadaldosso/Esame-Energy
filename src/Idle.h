#pragma once

#include "ofMain.h"

// definition of the Idle state class

class Idle
{
    
public:
    
    Idle();
    void setup();
    void update();
    void draw();
    
    ofImage logo;
};
