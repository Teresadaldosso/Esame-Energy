#include "Idle.h"

// constructor

Idle::Idle()
{
    
}

// set and draw the image for the idle state

void Idle::setup()
{
    logo.load("images/logoadidas.png");
    logo.resize(ofGetWidth(), ofGetHeight());
}

void Idle::update()
{
    
}

void Idle::draw()
{
    logo.draw(0,0);
}
