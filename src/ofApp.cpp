#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup()
{
    serial.listDevices();
    serial.setup(0, 9600);
    
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    ofSetBackgroundColor(0);
    ofSetCircleResolution(360);
    
    idle = new Idle();
    //setup of the idle state object
    idle->setup();
    
    detect = new Detection();
    //setup of the detection obeject
    detect->setup();
    
    //set the starting machine state to IDLE
    mode = IDLE;
    
    
    ofSetFrameRate(60);
    ofBackground(0);
    mesh.setMode(OF_PRIMITIVE_POINTS);
    
    //boolean control to apply some functions to the particle system
    pressed = false;
    
    // set the particle system starting position to random positions
    for (int i = 0; i < num; i++) {
        particles[i].position = ofVec3f(ofRandom(ofGetWidth()), ofRandom(ofGetHeight()), ofRandom(-ofGetWidth(), ofGetWidth()));
        particles[i].depth = ofGetWidth();
    }
}


//--------------------------------------------------------------
void ofApp::update()
{
    detect->update();
    
    forceNoise = ofNoise(ofGetElapsedTimef() * 1.2f);
    
    mesh.clear();
    
    
    // for loop into the entire particle system size
    for(int i = 0; i < num; i++)
    {
        //boolean control to apply the attraction force to our particles
        if (pressed)
        {
            particles[i].addAttractionForce(mouseX, mouseY, 0, ofGetWidth() * 1.5, 1.0);
        }
        
        particles[i].update();
        particles[i].throughOffWalls();
        
        //add a mesh
        mesh.addVertex(ofVec3f(particles[i].position.x, particles[i].position.y, particles[i].position.z));
    }

    
      //setting the Arduino inputs
    
    if(serial.available() && bReceiveFromArduino)
    {
        unsigned char bytesReturned[3];
        
        serial.readBytes(bytesReturned, 4);
        
        string serialData = (char*) bytesReturned;
        forceValue = ofToInt(serialData);
        
        
        serial.flush();
    }
}


//--------------------------------------------------------------

//defining the function drawParticles to apply the mesh
void ofApp::drawParticles()
{
    ofSetColor(64, 255, 255, 192);
    ofEnableBlendMode(OF_BLENDMODE_ADD);
    static GLfloat distance[] = { 0.0, 0.0, 1.0 };
    glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, distance);
    glPointSize(500);
    mesh.draw();
    ofDisableBlendMode();
}
//--------------------------------------------------------------

void ofApp::draw()
{
    setState(mode);
}

 //set the switch-statement for the draw functions of our state machine

void ofApp::setState(StateMode m)
{
    switch (m)
    {
        case IDLE:
            bReceiveFromArduino = false;
            idle->draw();
            break;
            
        case ENGANGEMENT:
            detect->draw();
            break;
            
        case PLAY:
            bReceiveFromArduino = true;
            drawParticles();
            break;
            
        default:
            break;
    }
}

//--------------------------------------------------------------

void ofApp::keyPressed(int key)
{
  
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
    //set the switch-statement to control the state machine through the inputs of the keyboard
    switch (key)
    {
        case '1':
            mode = IDLE;
            break;
            
        case '2':
            mode = ENGANGEMENT;
            break;
            
        case '3':
            mode = VIDEO;
            break;
            
        case '4':
            mode = PLAY;
            break;
            
        default:
            break;
    }

}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    pressed = true;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    pressed = false;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
