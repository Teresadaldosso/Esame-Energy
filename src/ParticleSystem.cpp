#include "ParticleSystem.h"

//declaration of the setup values of the particle system

ParticleSystem::ParticleSystem(){
    radius = ofRandom(5.0,10);
    
    // definition of a friction value to simulate the friction force in the particles' movement
    friction = 0.01;
    
    mass = 1.0;
    position.set(ofGetWidth()/2.0, ofGetHeight()/2.0, 0);
    velocity.set(0, 0, 0);
    acceleration.set(0, 0, 0);
    minx = 0;
    miny = 0;
    minz = -ofGetHeight();
    maxx = ofGetWidth();
    maxy = ofGetHeight();
    maxz = ofGetHeight();
}

void ParticleSystem::update(){
    acceleration -= velocity * friction;
    velocity += acceleration;
    position += velocity;
   
    // set the particles so that they are not moving at the beginning
    acceleration.set(0, 0, 0);
}

void ParticleSystem::addForce(ofVec3f _force){
    
    // second law of dynamics
    acceleration += _force / mass;
}

    //overloading the addForce function
void ParticleSystem::addForce(float forceX, float forceY, float forceZ){
    
    // second law of dynamics
    acceleration += ofVec3f(forceX, forceY, forceZ) / mass;
}

    //set the particles' movement so that they bounce on the fixed limits (three dimensions)
void ParticleSystem::bounceOffWalls(){
    if (position.x > maxx){
        position.x = maxx;
        velocity.x *= -1;
    }
    if (position.x < minx){
        position.x = minx;
        velocity.x *= -1;
    }
    if (position.y > maxy){
        position.y = maxy;
        velocity.y *= -1;
    }
    if (position.y < miny){
        position.y = miny;
        velocity.y *= -1;
    }
    if (position.z > maxz){
        position.z = maxz;
        velocity.z *= -1;
    }
    if (position.z < minz){
        position.z = minz;
        velocity.z *= -1;
    }
}

//second option to check border

void ParticleSystem::throughOffWalls(){
    if (position.x < minx) {
        position.x = maxx;
    }
    if (position.y < miny) {
        position.y = maxy;
    }
    if (position.z < minz) {
        position.z = maxz;
    }
    if (position.x > maxx) {
        position.x = minx;
    }
    if (position.y > maxy) {
        position.y = miny;
    }
    if (position.z > maxz) {
        position.z = minz;
    }
}

//reset a random position for the particles after the pressure

void ParticleSystem::resetToRandomPos(){
    float minx = 0;
    float miny = 0;
    float maxx = ofGetWidth();
    float maxy = ofGetHeight();
    float maxz = depth;
    float minz = -depth;
    if (position.x < minx || position.y < miny || position.z < minz || position.x > maxx || position.y > maxy || position.z > maxz) {
        position.set(ofRandom(minx, maxx), ofRandom(miny, maxy), ofRandom(minz, maxz));
    }
}


//add an attraction force

void ParticleSystem::addAttractionForce(float x, float y, float z, float radius, float scale){
    ofVec3f posOfForce;
    posOfForce.set(x,y,z);
    ofVec3f diff = position - posOfForce;
    float length = diff.length();
    bool bAmCloseEnough = true;
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    if (bAmCloseEnough == true){
        float pct = 1 - (length / radius);
        diff.normalize();
        acceleration = acceleration - diff * scale * pct;
    }
}

//overloading the function addAttractionForce taking by reference an object of the ParticleSystem type

void ParticleSystem::addAttractionForce(ParticleSystem &p, float radius, float scale){
    ofVec3f posOfForce;
    posOfForce.set(p.position.x, p.position.y, p.position.z);
    ofVec3f diff = position - posOfForce;
    float length = diff.length();
    bool bAmCloseEnough = true;
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    if (bAmCloseEnough == true){
        float pct = 1 - (length / radius);
        diff.normalize();
        acceleration = acceleration - diff * scale * pct;
    }
}


// add a repulsion force (second option)

void ParticleSystem::addRepulsionForce(float x, float y, float z, float radius, float scale){
    ofVec3f posOfForce;
    posOfForce.set(x,y,z);
    ofVec3f diff = position - posOfForce;
    float length = diff.length();
    bool bAmCloseEnough = true;
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    if (bAmCloseEnough == true){
        float pct = 1 - (length / radius);
        diff.normalize();
        acceleration = acceleration + diff * scale * pct;
    }
}

 //overloading the function addRepulsionForce (second option) 

void ParticleSystem::addRepulsionForce(ParticleSystem &p, float radius, float scale){
    ofVec3f posOfForce;
    posOfForce.set(p.position.x, p.position.y, p.position.z);
    ofVec3f diff = position - posOfForce;
    float length = diff.length();
    bool bAmCloseEnough = true;
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    if (bAmCloseEnough == true){
        float pct = 1 - (length / radius);
        diff.normalize();
        acceleration = acceleration + diff * scale * pct;
    }
}


